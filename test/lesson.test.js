import Lesson from '../resources/assets/js/models/Lesson';
import chai from 'chai';

const assert = chai.assert;

describe('Lesson', () => {
    let attributes = {
        id: 1,
        title: 'title',
        description: 'description',
        created_at: '2016-10-29 01:52:23',
        updated_at: '2017-10-30 02:42:11',
    };
    let newAttributes = {
        id: 2,
        title: 'title1',
        description: 'description2',
        created_at: '2016-11-29 01:52:23',
        updated_at: '2017-11-30 02:42:11',
    };
    let lesson;

    it('Атрибуты устанавливаются при создании нового объекта', () => {
        lesson = new Lesson(attributes);
        assert.equal(lesson.id, attributes.id);
        assert.equal(lesson.title, attributes.title);
        assert.equal(lesson.description, attributes.description);
        assert.equal(lesson.created_at, attributes.created_at);
        assert.equal(lesson.updated_at, attributes.updated_at);
    });

    it('Атрибуты меняются', () => {
        var assert = chai.assert;
        lesson = new Lesson(attributes);

        lesson.id = newAttributes.id;
        lesson.title = newAttributes.title;
        lesson.description = newAttributes.description;
        lesson.created_at = newAttributes.created_at;
        lesson.updated_at = newAttributes.updated_at;

        assert.equal(lesson.id, newAttributes.id);
        assert.equal(lesson.title, newAttributes.title);
        assert.equal(lesson.description, newAttributes.description);
        assert.equal(lesson.created_at, newAttributes.created_at);
        assert.equal(lesson.updated_at, newAttributes.updated_at);
    });
});
