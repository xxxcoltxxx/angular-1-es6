<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/assets/all.css" />

        <title>Lessons</title>
    </head>
    <body ng-app="app">
        <div class="container">
            <div ng-controller="HomeCtrl">
                <div class="lessons">
                    <ul>
                        <li ng-repeat="lesson in lessons">
                            <span class="status">
                                <i class="fa fa-check-circle-o"></i>
                            </span>
                            <span class="title">
                                <b>@{{ lesson.title }}</b>: @{{ lesson.description }}
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer">
            Copyright 2016
        </div>

    <script type="application/javascript" src="/assets/all.js"></script>
    </body>
</html>
