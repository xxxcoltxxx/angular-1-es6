export default class Lesson {
    constructor(attributes) {
        this.id = attributes.id || null;
        this.title = attributes.title || '';
        this.description = attributes.description || '';
        this.created_at = attributes.created_at || null;
        this.updated_at = attributes.updated_at || null;
    }

    cast() {
        this.id = this.id + 0;
    }

    test(...args) {
        console.log(this, ...args);
    }
}

Lesson.$inject = ['$http'];
