export default class HomeCtrl {

    /**
     * @param {$scope} $scope
     * @param {$timeout} $timeout
     * @param {LessonsService} LessonsService
     */
    constructor($scope, $timeout, LessonsService) {
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.lessonService = LessonsService;

        this.init();
    }

    init() {
        this.$scope.lessons = [];

        this.loadLessons();
    }

    /**
     * Загрузка списка уроков
     */
    loadLessons() {
        this.lessonService.get()
            .then(lessons => this.$timeout(() => {
                this.$scope.lessons = lessons
            }));
    }
}

HomeCtrl.$inject = ['$scope', '$timeout', 'LessonsService'];