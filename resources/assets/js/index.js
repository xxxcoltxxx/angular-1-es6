import angular from 'angular';
import HomeCtrl from './controllers/HomeCtrl';
import LessonsService from './services/LessonsService';


angular.module('app', [])
    .controller('HomeCtrl', HomeCtrl)
    .service('LessonsService', LessonsService)
;