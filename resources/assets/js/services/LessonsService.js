import Lesson from '../models/Lesson';
var Route = require('url-template');

const API_GET = '/api/v1/lessons/{id}';
const API_LIST = '/api/v1/lessons';

export default class LessonsService {

    /**
     * @param {$http} $http
     */
    constructor($http) {
        this.$http = $http;
    }

    /**
     * Поиск урока
     *
     * @param lesson_id
     * @returns {Promise}
     */
    find(lesson_id) {
        return new Promise((resolve, reject) => {
            this.$http.get(Route.parse(API_GET).expand({id: lesson_id}))
                .then(response => {
                    resolve(new Lesson(response.data))
                })
                .catch(e => reject(e))
        });
    }

    /**
     * Получает список уроков
     *
     * @param params
     * @returns {Promise}
     */
    get(params = []) {
        return new Promise((resolve, reject) => {
            let lessons = [];

            this.$http.get(API_LIST, {params: params})
                .then(response => {
                    response.data.forEach(attributes => lessons.push(new Lesson(attributes)));

                    lessons.forEach(lesson => console.log(lesson.test(1, 2, 3)));

                    resolve(lessons);
                })
                .catch(e => reject(e));
        });
    }
}

LessonsService.$inject = ['$http'];
