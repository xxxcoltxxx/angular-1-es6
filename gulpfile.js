const elixir = require('laravel-elixir');

elixir((mix) => {
    mix.browserify(['index.js'], 'public/assets/all.js')
        .less(['vars.less', 'styles/**/*.less'], 'public/assets/less.css');

    mix.styles([
            'node_modules/font-awesome/css',
            'public/assets/less.css'
        ], 'public/assets/all.css', './');

    mix.copy('node_modules/font-awesome/fonts', 'public/fonts/');
});
