<?php


namespace App\Models;


/**
 * App\Models\Lesson
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Lesson whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Lesson whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Lesson whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Lesson whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Lesson whereUpdatedAt($value)
 */
class Lesson extends \Eloquent
{

}