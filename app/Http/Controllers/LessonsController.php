<?php


namespace App\Http\Controllers;


use App\Models\Lesson;


class LessonsController extends Controller
{
    public function index()
    {
        return Lesson::take(10)->get();
    }

    public function show($id)
    {
        return Lesson::find($id);
    }
}